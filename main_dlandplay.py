import os
import ffmpeg
import discord
from discord.ext import commands

import threadmanager

START_COMMAND = "play"
CHANNEL_NAME = 'commandes-bot'
GUILD_NAME = 'SPECTRE'
TOKEN_DISCORD = ''

voice_client = None

bot = commands.Bot(command_prefix='$')

def is_message_valid(message):
    return message.author != bot.user and message.channel.name == CHANNEL_NAME

async def reset(filename=None):
    global voice_client
    try:
        voice_client.stop()
        await voice_client.disconnect()
    except TypeError as e:
        print(e)
        pass
    voice_client = None
    if filename is not None:
        os.remove(filename)

#----------------------------------------------------------------
#----------------------GESTION DES CHANNELS----------------------
#----------------------------------------------------------------

# Recupere tous les channels du server
# qui sont des channels VOCAUX et qui on AU MOINS UNE personne de co dessus
def get_channels(bot):
    guild = [x for x in bot.guilds if x.name == GUILD_NAME][0]
    channels = [c for c in guild.channels if 'voice' in c.type and len(c.voice_states) > 0]
    return channels

#Retrouve dans quel channel et le user en param
async def find_channel_by_user_id(bot, user_id):
    channels = get_channels(bot)
    for channel in channels:
        for voice in channel.voice_states:
            if voice == user_id:
                return channel

#----------------------------------------------------------------
#-------------------------GESTION DU SON-------------------------
#----------------------------------------------------------------

# Créer un voice_client
async def start_voice_client(bot, channel):
    channel = await bot.fetch_channel(channel.id)
    return await channel.connect()

# Tout le processs d'envoi de son
async def play_vocal(bot, vocal, channel):
    global voice_client
    voice_client = await start_voice_client(bot, channel)
    source = discord.FFmpegPCMAudio(vocal)
    voice_client.play(source)

#----------------------------------------------------------------
#----------------------------COMMANDE----------------------------
#----------------------------------------------------------------

@bot.command(name=START_COMMAND)
async def start_command(ctx):
    global bot, voice_client
    if not is_message_valid(ctx):
        return

    filename = ctx.message.attachments[0].filename
    await ctx.message.attachments[0].save(fp=filename)
    channel = await find_channel_by_user_id(bot, ctx.author.id)
    await play_vocal(bot, filename, channel)

    await ctx.message.delete()
    duration = ffmpeg.probe(filename)['format']['duration']
    print(duration)
    await threadmanager.start(duration, voice_client, filename)

# permet de mute le bot
@bot.command(name='stop')
async def start_command(ctx):
    if is_message_valid(ctx):
        print('STOPPED')
        await reset()

#handler error
@bot.event
async def on_command_error(context, exception):
    print(exception)

# Lancement du bot
bot.run(TOKEN_DISCORD)

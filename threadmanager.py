import threading
import asyncio
import time
import math
import os

class DisconnectThread:
    def __init__(self, duration, voice_client, filename): 
        threading.Thread.__init__(self)
        self.duration = duration
        self.voice_client = voice_client
        self.filename = filename

    async def run(self):
        await asyncio.sleep(math.ceil(float(self.duration)))
        await self.voice_client.disconnect()
        os.remove(self.filename)

async def start(duration, voice_client, filename):
    thread = DisconnectThread(duration, voice_client, filename)
    await thread.run()
